#include "bmp.h"

#include <inttypes.h>
#include <stdio.h>
#include <stdbool.h>

#define PRI_SPECIFIER(e) (_Generic( (e), uint16_t : "%" PRIu16, uint32_t: "%" PRIu32, default: "NOT IMPLEMENTED" ))

#define PRINT_FIELD(t, name) \
    fprintf( f, "%-17s: ",  # name ); \
    fprintf( f, PRI_SPECIFIER( header-> name ) , header-> name );\
    fprintf( f, "\n");


void bmp_header_print(struct bmp_header const *header, FILE *f) {
    FOR_BMP_HEADER(PRINT_FIELD)
}

void destruct_bmp_image(struct bmp_image *bmp) {
    free(bmp->data);
}

static struct bmp_header read_header(FILE *f) {
    struct bmp_header h = {0};
    fread(&h, sizeof(struct bmp_header), 1, f);
    if (ferror(f)) {
        h.bfType = 0; // обработается как invalid header
    }
    return h;
}

void skip_bytes(FILE *f, size_t count) {
    unsigned char t;
    for (int i = 0; i < count; i++) {
        fread(&t, 1, 1, f);
    }
}

bool read_body(FILE *f, struct bmp_image *bmp) {
    skip_bytes(f, bmp->header.bOffBits - bmp->readen_bytes);
    if (bmp->header.biCompression != 0) {
        return false;
    }
    struct pixel *pixels = malloc(bmp->header.biHeight * bmp->header.biWidth * sizeof(struct pixel));
    for (size_t i = 0; i < bmp->header.biHeight; i++) {
        fread(&pixels[i * bmp->header.biWidth], sizeof(struct pixel), bmp->header.biWidth, f);
        if (ferror(f) || feof(f)) {
            free(pixels);
            destruct_bmp_image(bmp);
            return false;
        }
        skip_bytes(f, (4 - bmp->header.biWidth * sizeof(struct pixel) % 4) % 4);
    }
    bmp->data = pixels;
    return true;
}

void bmp_to_image(struct bmp_image const *bmp, struct image *img) {
    img->width = bmp->header.biWidth;
    img->height = bmp->header.biHeight;
    img->data = malloc(img->width * img->height * sizeof(struct pixel));
    for (size_t i = 0; i < img->height; i++) {
        for (size_t j = 0; j < img->width; j++) {
            img->data[j + i * img->width] = bmp->data[i * img->width + j];
        }
    }
}

struct bmp_header gen_header(uint64_t width, uint64_t height) {
    struct bmp_header header;
    header.biCompression = 0;
    header.bfType = 19778; // ascii "BM"
    header.bfReserved = 0;
    header.bOffBits = sizeof(struct bmp_header);
    header.biSize = 40;
    header.biWidth = width;
    header.biHeight = height;
    header.biPlanes = 1;
    header.biBitCount = 24;
    uint64_t row_size = header.biWidth * header.biBitCount / 8;
    row_size += row_size % 4;
    header.bfileSize = header.bOffBits
                       + row_size * header.biHeight
                       + (4 - row_size * header.biHeight % 4) % 4;
    return header;
}

struct bmp_image *image_to_bmp(struct image const *img) {
    struct bmp_image *bmp = malloc(sizeof(struct bmp_image));
    bmp->header = gen_header(img->width, img->height);
    struct pixel *pixels = malloc(bmp->header.biWidth * bmp->header.biHeight * sizeof(struct pixel));

    for (size_t i = 0; i < bmp->header.biHeight; i++) {
        for (size_t j = 0; j < bmp->header.biWidth; j++) {
            pixels[i * bmp->header.biWidth + j] = img->data[i * bmp->header.biWidth + j];
        }
    }
    bmp->data = pixels;
    return bmp;
}

enum read_status read_bmp(FILE *in, struct image *image) {
    struct bmp_image bmp = {0};
    if (!in)
        return READ_FILE_CLOSED;

    bmp.header = read_header(in);

    bmp.readen_bytes = sizeof(bmp.header);

    if (bmp.header.bfType != 19778) {
        return READ_INVALID_HEADER;
    }

    if (!read_body(in, &bmp)) {
        return READ_INVALID_DATA;
    }

    bmp_to_image(&bmp, image);
    destruct_bmp_image(&bmp);

    return READ_OK;
}


enum write_status write_bmp(FILE *out, struct image const *image) {
    if (!out)
        return WRITE_FILE_CLOSED;
    struct bmp_image *bmp = image_to_bmp(image);
    fwrite(&bmp->header, sizeof(struct bmp_header), 1, out);
    skip_bytes(out, bmp->header.bOffBits - sizeof(bmp->header));
    for (size_t i = 0; i < bmp->header.biHeight; i++) {
        fwrite(&bmp->data[i * bmp->header.biWidth], sizeof(struct pixel), bmp->header.biWidth, out);
        skip_bytes(out, (4 - bmp->header.biWidth * sizeof(struct pixel) % 4) % 4);
    }
    destruct_bmp_image(bmp);
    return WRITE_OK;
}


