//
// Created by yank0vy3rdna on 13.01.2021.
//

#include "image.h"

void destruct_image(struct image *img) {
    free(img->data);
}