//
// Created by yank0vy3rdna on 13.01.2021.
//

#ifndef ASSIGNMENT_IMAGE_ROTATION_ROTATE_H
#define ASSIGNMENT_IMAGE_ROTATION_ROTATE_H
#include "../image.h"

void rotate(struct image* img);

#endif //ASSIGNMENT_IMAGE_ROTATION_ROTATE_H
