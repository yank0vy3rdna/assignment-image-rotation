//
// Created by yank0vy3rdna on 13.01.2021.
//

#include "rotate.h"

void rotate(struct image *img) {
    uint32_t r = img->height;
    img->height = img->width;
    img->width = r;

    struct pixel *new_pixels = malloc(img->width * img->height * sizeof(struct pixel));
    for (size_t i = 0; i < img->height; i++) {
        for (size_t j = 0; j < img->width; j++) {
            new_pixels[i * img->width + j] = img->data[(img->width - j) * img->height + i];
        }
    }

    free(img->data);
    img->data = new_pixels;

}