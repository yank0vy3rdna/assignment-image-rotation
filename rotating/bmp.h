#ifndef _BMP_H_
#define _BMP_H_

#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>
#include <stdlib.h>
#include "image.h"

#define FOR_BMP_HEADER(FOR_FIELD) \
        FOR_FIELD( uint16_t,bfType)\
        FOR_FIELD( uint32_t,bfileSize)\
        FOR_FIELD( uint32_t,bfReserved)\
        FOR_FIELD( uint32_t,bOffBits)\
        FOR_FIELD( uint32_t,biSize)\
        FOR_FIELD( uint32_t,biWidth)\
        FOR_FIELD( uint32_t,biHeight)\
        FOR_FIELD( uint16_t,biPlanes)\
        FOR_FIELD( uint16_t,biBitCount)\
        FOR_FIELD( uint32_t,biCompression)\
        FOR_FIELD( uint32_t,biSizeImage)\
        FOR_FIELD( uint32_t,biXPelsPerMeter)\
        FOR_FIELD( uint32_t,biYPelsPerMeter)\
        FOR_FIELD( uint32_t,biClrUsed)\
        FOR_FIELD( uint32_t,biClrImportant)

#define DECLARE_FIELD(t, n) t n ;

struct __attribute__((packed)) bmp_header {
    FOR_BMP_HEADER(DECLARE_FIELD)
};


struct bmp_image {
    DECLARE_FIELD(struct bmp_header, header)
    DECLARE_FIELD(struct pixel *, data)
    DECLARE_FIELD(size_t, readen_bytes)
};

//void destruct_bmp_image(struct bmp_image *bmp);

//void bmp_header_print(struct bmp_header const *header, FILE *f);

FILE *open_file(const char *filename, const char *modes);

/*  deserializer   */
enum read_status {
    READ_OK,
    READ_INVALID_HEADER,
    READ_FILE_CLOSED,
    READ_INVALID_DATA,
};

enum read_status read_bmp(FILE *in, struct image *bmp);

/*  serializer   */
enum write_status {
    WRITE_OK,
    WRITE_FILE_CLOSED,
    WRITE_ERROR,
    /* коды других ошибок  */
};

enum write_status write_bmp(FILE *out, struct image const *image);


#endif
