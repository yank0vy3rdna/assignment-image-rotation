//
// Created by yank0vy3rdna on 12.01.2021.
//

#ifndef ASSIGNMENT_IMAGE_ROTATION_IMAGE_H
#define ASSIGNMENT_IMAGE_ROTATION_IMAGE_H
#include <stdint.h>
#include <stdlib.h>


struct image {
    uint64_t width, height;
    struct pixel *data;
};

struct pixel {
    uint8_t b, g, r;
};

void destruct_image(struct image *img);

#endif //ASSIGNMENT_IMAGE_ROTATION_IMAGE_H