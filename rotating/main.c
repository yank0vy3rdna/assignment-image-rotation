#include <string.h>
#include "transformations/rotate.h"

#include "bmp.h"
#include "util.h"
#include "file.h"


void usage() {
    fprintf(stderr,
            "Usage: ./assignment_image_rotation BMP_FILE_NAME [ optional: out ]. stdin stdout usages are possible\n");
}

int main(int argc, char **argv) {
    if (argc != 2 && argc != 3) usage();
    if (argc < 2) err("Not enough arguments \n");
    if (argc > 3) err("Too many arguments \n");
    FILE *file;
    FILE *out;
    if (strcmp(argv[1], "stdin") == 0) {
        file = stdin;
        if (argc != 3){
            err("You should specify output file when you use stdin\n");
        }
    } else {
        file = open_file(argv[1], "rb");
    }


    struct image img = {0};
    switch (read_bmp(file, &img)) {
        case READ_OK:
            break;
        case READ_INVALID_HEADER:
            fclose(file);
            destruct_image(&img);
            err("Failed to read header\n");
        case READ_FILE_CLOSED:
            fclose(file);
            destruct_image(&img);
            err("Failed to open file\n");
        case READ_INVALID_DATA:
            fclose(file);
            destruct_image(&img);
            err("Failed to read file data\n");
        default:
            fclose(file);
            destruct_image(&img);
            err("Unknown error\n");
    }

    rotate(&img);


    close_file(file);

    if (argc == 3) {
        if (strcmp(argv[2], "stdout") == 0) {
            out = stdout;
        } else {
            out = open_file(argv[2], "wb");
        }
    } else {
        out = open_file(argv[1], "wb");
    }

    switch (write_bmp(out, &img)) {
        case WRITE_OK:
            break;
        case WRITE_FILE_CLOSED:
            fclose(out);
            destruct_image(&img);
            err("Failed to open file\n", out);
        default:
            fclose(out);
            destruct_image(&img);
            err("Unknown error\n", out);
    }

    destruct_image(&img);

    close_file(out);
    return 0;
}
