//
// Created by yank0vy3rdna on 12.01.2021.
//

#ifndef ASSIGNMENT_IMAGE_ROTATION_FILE_H
#define ASSIGNMENT_IMAGE_ROTATION_FILE_H

#include <stdio.h>

FILE *open_file(const char *filename, const char *modes) {
    if (!filename) return 0;
    FILE *f = fopen(filename, modes);
    return f;
}
void close_file(FILE * file) {
    if (file) fclose(file);
}

#endif //ASSIGNMENT_IMAGE_ROTATION_FILE_H
